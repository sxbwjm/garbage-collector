#include <stdio.h>
#include <stdlib.h>




extern int __data_start;
extern int _end;


int memInitialize(unsigned int size);
void *memAllocate(unsigned int size, void (*finalize)(void *));
void memDump(void);
int arr[10];
void collectGarbage();

void finalizer(void* par)
{
  printf("this block is freed: start_addr - %08x\n", (int)par);
}

int main()
{
  /*
  int word = 0x12345678;
  setAllocBit(&word);
  printf("%08x %d\n", word, getAllocBit(&word));
  clearAllocBit(&word);
  printf("%08x %d\n", word, getAllocBit(&word));

  setMarkBit(&word);
  printf("%08x %d\n", word, getMarkBit(&word));

  clearMarkBit(&word);
  printf("%08x %d\n", word, getMarkBit(&word));
*/
  
  if(memInitialize(200) == 0)
  {
    printf("mem initialize fail\n");
    return 0;
  }
  
    //memDump();

 // printf("heapStart:%08x end: %08x\n",(int)_heap_start_addr, (int)(_heap_start_addr+_heap_size));
  //printf("%08x %08x\n", (int)&gHeapStartAddr,(int)gHeapStartAddr);
  int* mem1 = memAllocate(100,0);
  if(mem1 == 0)
  {
    printf("fail1\n");
    return 0;
  }
  //memDump();
  
  int* mem2 = memAllocate(20,(void*)finalizer);
  if(mem2 == 0)
  {
    printf("fail2\n");
    return 0;
  }
  //memDump();
  
  int* mem3 = memAllocate(70,(void*)finalizer);
  if(mem3 == 0)
  {
    printf("fail3\n");
    return 0;
  }
 // memDump();
  
  
  //int* mem3 = memAllocate(0,0);
  /*
  *mem1 = 0x1111;
  *(mem1+1) = 0x2222;
  *(mem1+2) = (int)mem2;
  *(mem2+10) = (int)mem1;
   */
  //*mem2 = (int)mem1;
 // mem1 = NULL;
  //*(mem3+1) = (int)(mem4+5);
  //mem1 = NULL;
  mem1 = NULL;
  //arr[2] = mem2;
  mem2 = NULL;
  //mem4 = NULL;

  memDump();
  int* mem4 = memAllocate(10,finalizer);
  if(mem4 == 0)
  {
    printf("fail4\n");
    return 0;
  }
  memDump();
 // *mem3 = 0x8293298;
  arr[0] = 0x0100;
  arr[1] = 0x0102;
  arr[2] = 0x0103;//(int)(mem1-2);
  
  
  arr[3] = arr[2] + 40;
  arr[4] = arr[3] + 40;
  arr[5] = arr[4] + 40;
  arr[6] = arr[5] + 1;
  arr[7] = arr[6] + 20;
  arr[8] = arr[7] + 20;
  arr[9] = arr[8] + 20;
  

  //collectGarbage();
  //__data_start = arr[0];
  //_end = arr[2];
  //printf("__data_start: %08x _end: %08x\n", (int)&__data_start,(int)&_end);

 // memDump();
  return 0;
}
