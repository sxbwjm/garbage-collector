/**************************************************************
 *                       Prog5                                *
 *                                                            *
 * Author:   Xiaobo Sun                                       *
 *                                                            *
 * Purpose:  conservative garbage collector                   *
 *                                                            *
 * Usage:                                                     *
 *     The goal of this assignment is to build a memory       *
 *     allocator that has an associated conservative garbage  *
 *     collector. The memory allocator will be designed and   *
 *     implemented for the Intel IA-32 architecture           *
 *                                                            *
 * Created:  11/08/2012                                       *
 **************************************************************/

#include <stdlib.h>
#include <stdio.h>

/************************************************************************
 *                        TYPE DEFINATION
 *************************************************************************/
typedef enum
{
  false = 0,
  true
} boolean;

/************************************************************************
 *                        GLOABLE VARIABLE DEFINATION
 *************************************************************************/
extern int __data_start;
extern int _end;

static int* gHeapStartAddr = 0;
static int gHeapSize = 0;

/************************************************************************
 *                      LOCAL FUNCTION DECLARATION
 *************************************************************************/
void memDump(void);
static void dumpGlobalMem();
static void dumpStack();
static void dumpRegister();
static void dumpHeap();
static int getAllocBit(int* blockHeader);
static void setAllocBit(int* blockHeader);
static void clearAllocBit(int* blockHeader);
static int getMarkBit(int* blockHeader);
static void setMarkBit(int* blockHeader);
static void clearMarkBit(int* blockHeader);
static int getBlockLen(int* blockHeader);
static void* getFinalizer(int* blockHeader);
static void createBlockHeader(int* addr, int len, int finalize);
static int* isHeapPointer(int* memAddr);
static void scanMemRangeForHeapPointer(int* memStartAddr, int* memEndAddr);
static void markBlocks();
static void freeUnmarkedBlocks();
static void clearAllMarkBits();
static void collectGarbage();

int* GC_GetFP();
int GC_GetEBX();
int GC_GetESI();
int GC_GetEDI();


/************************************************************************
 *                         memInitialize
 * initialize the allocator to hold the number of 32-bit words
 *
 * parameter:
 *    size - the length of the heap
 * return value: 
 *    0 - failure
 *    1 - success
 *************************************************************************/
int memInitialize(unsigned int size)
{
  //check the size
  if(size <= 0 || size > 0x3FFFFFFF)
  {
    return false;
  }
  
  //allocate memory
  gHeapStartAddr = malloc(sizeof(int) * size);
  if(gHeapStartAddr == NULL)
  {
    gHeapSize = 0;
    return false;
  }
  
  gHeapSize = size;
  
  //create block header
  createBlockHeader(gHeapStartAddr,size,0);
  
  return true;
}

/************************************************************************
 *                         memAllocate
 * allocates a contiguous block of 32-bit words of specified length
 *
 * parameters:
 *    size - the length of block
 *    finalize - the pointer of finalizer function
 * return value: the pointer that is pointed to the allocated block
 *************************************************************************/
void *memAllocate(unsigned int size, void (*finalize)(void *))
{
  int* blockPointer = NULL;
  int blockLen = 0;
  int garbageCollectFlg = 0;
  
  //check the parameter
  if(size > gHeapSize - 2 || size < 0)
  {
    return (void*)0;
  }
  
  if((int)finalize == (int)memAllocate)
  {
    fprintf(stderr, "Cannot use memAllocate as finalize function!\n");
    exit(-1);
  }
  

  size += 2;
  
  //loop at most two times(when there is no available free block for allocating)
  while(1)
  {
    blockPointer = gHeapStartAddr;
    //search for availble block
    while(blockPointer < gHeapStartAddr + gHeapSize)
    {
      blockLen = getBlockLen(blockPointer);
      
      //if alloc bit is 0 and block length is bigger than request size
      if( getAllocBit(blockPointer) == 0 && blockLen >= size )
      {
        break;
      }
      //point to the next block
      blockPointer += blockLen;
    }
  
    //if the availabe block is found
    if(blockPointer < gHeapStartAddr + gHeapSize)
    {
      //change the block header
      createBlockHeader(blockPointer, size, (int)finalize);
      setAllocBit(blockPointer);
      
      //create a new block for the rest part heap
      createBlockHeader(blockPointer+size, blockLen - size, 0);
      return blockPointer + 2;
    }
    else
    {
      //run the garbageCollector if it has not been run yet
      if(garbageCollectFlg == 0)
      {
        collectGarbage();
        garbageCollectFlg = 1;
      }
      //return 0 if there is still no available free block
      //for allocating after garbage collecting
      else
      {
        return (void*)0;
      }
    }
  }
  
  return (void*)0;
}



/************************************************************************
 *                         memDump
 * Dump the contents of memory
 *************************************************************************/
void memDump(void)
{  
  //Dump the global Memory
  dumpGlobalMem();
  
  //Dump the stack Memory
  dumpStack();
  
  //Dump the Registers
  dumpRegister();
    
  //Dump the heap
  dumpHeap();
}

/************************************************************************
 *                         dumpGlobalMem
 * Dump the global memory
 *************************************************************************/
static void dumpGlobalMem()
{
  int* memAddr = NULL;
  int count = 0;
  
  //global memory summary
  fprintf(stderr, "Global Memory: start=%08x end=%08x length=%d\n\n",
          (int)&__data_start, (int)&_end, &_end - &__data_start);
  
  count = 0;
  //address and contents of memory that is pointed to heap
  for(memAddr=&__data_start; memAddr<=&_end; memAddr++)
  {
    if(isHeapPointer(memAddr) != NULL)
    {
      fprintf(stderr, "%08x %08x\n", (int)memAddr, *memAddr);
      count++;
    }
  }
  
  if(count > 0)
  {
    fprintf(stderr, "\n");
  }
}

/************************************************************************
 *                         dumpStack
 * Dump the stack memory
 *************************************************************************/
static void dumpStack()
{
  int* memAddr = NULL;
  int* stackAddrStart = NULL;
  int* stackAddrEnd = NULL;  
  int count = 0;
  
  fprintf(stderr, "Stack Memory: ");
  stackAddrStart = GC_GetFP();
  if(stackAddrStart == NULL)
  {
    fprintf(stderr, "Cannot get the stack memory address\n\n");
  }
  else
  {
    //skip the the stack frame of this function
    stackAddrStart = (int*)*stackAddrStart;
    //look for the stack bottom
    stackAddrEnd = stackAddrStart;
    while( *stackAddrEnd != 0)
    {
      stackAddrEnd = (int*)*stackAddrEnd;
    }
    
    //stack summary
    fprintf(stderr, "start=%08x end=%08x length=%d\n\n",
            (int)stackAddrStart, (int)stackAddrEnd, stackAddrEnd - stackAddrStart);
    
    //address and contents of stack that is pointed to heap
    for(memAddr=stackAddrStart; memAddr<=stackAddrEnd; memAddr++)
    {
      if(isHeapPointer(memAddr) != NULL)
      {
        fprintf(stderr, "%08x %08x\n", (int)memAddr, *memAddr);
        count++;
      }
    }
    
    if(count > 0)
    {
      fprintf(stderr, "\n");
    }
    
  }
}

/************************************************************************
 *                         dumpRegister
 * Dump the registers
 *************************************************************************/
static void dumpRegister()
{
  int regValue = 0;
  
  fprintf(stderr, "Registers\n\n");
  //ebx
  regValue = GC_GetEBX();
  fprintf(stderr, "ebx %08x%s ",
          regValue, isHeapPointer(&regValue) == NULL ? " " : "*");
  //esi
  regValue = GC_GetESI();
  fprintf(stderr, "esi %08x%s ",
          regValue, isHeapPointer(&regValue) == NULL ? " " : "*");
  //edi
  regValue = GC_GetEDI();
  fprintf(stderr, "edi %08x%s \n\n",
          regValue, isHeapPointer(&regValue) == NULL ? " " : "*");

}

/************************************************************************
 *                         dumpHeap
 * Dump the stack memory
 *************************************************************************/
static void dumpHeap()
{
  int* heapAddr = NULL;
  int* blockHeaderAddr = NULL;
  int count = 0;

  fprintf(stderr, "Heap\n\n");
  
  blockHeaderAddr = gHeapStartAddr;
  while(blockHeaderAddr < gHeapStartAddr + gHeapSize)
  {
    //block summary(free block)
    if(getAllocBit(blockHeaderAddr) == 0)
    {
      fprintf(stderr, "Block %d Free\n", getBlockLen(blockHeaderAddr));
    }
    //block summary(allocated block)
    else
    {
      fprintf(stderr, "Block %d Allocated %s %08x\n",
              getBlockLen(blockHeaderAddr)-2,
              getMarkBit(blockHeaderAddr) == 1 ? "Marked" : "Unmarked",
              (int)getFinalizer(blockHeaderAddr)
              );
      //contents of the block
      heapAddr = blockHeaderAddr+2;
      count = 0;
      while(heapAddr < blockHeaderAddr + getBlockLen(blockHeaderAddr))
      {
        //heap address
        if(count%7 == 0)
        {
          fprintf(stderr, "%08x  ",(int)heapAddr);
        }
        
        //heap content
        fprintf(stderr, "%08x%s ",
                *heapAddr, isHeapPointer(heapAddr) == NULL ? " " : "*");
        
        count++;
        heapAddr++;
        
        //new line
        if(count%7 == 0)
        {
          fprintf(stderr, "\n");
        }
      }
      
      //adjust the new line
      if(count%7 != 0)
      {
        fprintf(stderr, "\n");
      }
    }
    
    fprintf(stderr, "\n");
    blockHeaderAddr += getBlockLen(blockHeaderAddr);
  }  
}

/************************************************************************
 *                         getAllocBit
 * get the alloc bit of block header
 *************************************************************************/
static int getAllocBit(int* blockHeader)
{
  return (*blockHeader & 0x80000000) == 0x80000000 ? 1 : 0;
}

/************************************************************************
 *                         setAllocBit
 * set the alloc bit of block header to 1
 *************************************************************************/
static void setAllocBit(int* blockHeader)
{
  *blockHeader = *blockHeader | 0x80000000;
}

/************************************************************************
 *                         clearAllocBit
 * clear the alloc bit of block header to 0
 *************************************************************************/
static void clearAllocBit(int* blockHeader)
{
  *blockHeader = *blockHeader & 0x7fffffff;
}

/************************************************************************
 *                         getMarkBit
 * get the mark bit of block header
 *************************************************************************/
static int getMarkBit(int* blockHeader)
{
  return (*blockHeader & 0x40000000) == 0x40000000 ? 1 : 0;
}

/************************************************************************
 *                         setMarkBit
 * set the mark bit of block header to 1
 *************************************************************************/
static void setMarkBit(int* blockHeader)
{
  *blockHeader = *blockHeader | 0x40000000;
}

/************************************************************************
 *                         clearMarkBit
 * clear the mark bit of block header to 0
 *************************************************************************/
static void clearMarkBit(int* blockHeader)
{
  *blockHeader = *blockHeader & 0xbfffffff;
}


/************************************************************************
*                         getBlockLen
* get the length of block from block header
*************************************************************************/
static int getBlockLen(int* blockHeader)
{
  return *blockHeader & 0x3FFFFFFF;
}


/************************************************************************
 *                         getFinalizer
 * get the pointer of finalizer
 *************************************************************************/
static void* getFinalizer(int* blockHeader)
{
  return (void*)*(blockHeader+1);
}


/************************************************************************
 *                         createNewBlockHeader
 * create a new block header
 *************************************************************************/
static void createBlockHeader(int* addr, int len, int finalize)
{
  *addr = len;
  *(addr+1) = finalize;
}

/************************************************************************
 *                         isHeapPointer
 * check if a pointer is pointed to the heap
 * return the address of the block header if it is pointed to the heap
 *************************************************************************/
static int* isHeapPointer(int* memAddr)
{
  int* blockHeaderAddr = NULL;
  int* nextBlockHeaderAddr = NULL;
  
  if(*memAddr >= (int)gHeapStartAddr && *memAddr < (int)(gHeapStartAddr + gHeapSize))
  {
    //non-pointer if pointed to the start address of heap
    if(*memAddr == (int)gHeapStartAddr)
    {
      return NULL;
    }
    
    blockHeaderAddr = gHeapStartAddr;
    //search for the block that is pointed to
    while(blockHeaderAddr < gHeapStartAddr + gHeapSize)
    {     
      nextBlockHeaderAddr = blockHeaderAddr + getBlockLen(blockHeaderAddr);

      //if pointed to the address from current block header + 1 to next block header
      if(*memAddr <= (int)nextBlockHeaderAddr )
      {
        if(getAllocBit(blockHeaderAddr) == 1)
        {
          return blockHeaderAddr;
        }
      }
      
      blockHeaderAddr = nextBlockHeaderAddr;
    }
  }
 
  return NULL;
}

/************************************************************************
 *                         scanMemRangeForHeapPointer
 * check a memory range to find heap pointers that are pointed to 
 * the allocated blocks that are in use and set their mark bits to 1
 *************************************************************************/
static void scanMemRangeForHeapPointer(int* memStartAddr, int* memEndAddr)
{
  int* memAddr = NULL;
  int* blockHeaderAddr = NULL;

  //scan the memory range
  for(memAddr=memStartAddr; memAddr<=memEndAddr; memAddr++)
  {
    //if pointed to an allocated heap block
    if( ( blockHeaderAddr = isHeapPointer(memAddr) ) != NULL)
    {
      
      //if the block is not marked
      if(getMarkBit(blockHeaderAddr) == 0)
      {
        setMarkBit(blockHeaderAddr);
        scanMemRangeForHeapPointer(blockHeaderAddr + 2, blockHeaderAddr + getBlockLen(blockHeaderAddr) - 1);
      }
    }
  }

}

/************************************************************************
 *                         markBlocks
 * identify allocated blocks that are in use and set their mark bits to 1
 *************************************************************************/
static void markBlocks()
{
  int* stackAddrStart = NULL;
  int* stackAddrEnd = NULL;
  int regValue = 0;
  
  //scan the global memory
  scanMemRangeForHeapPointer(&__data_start, &_end);
  
  //scan the stack
  stackAddrStart = GC_GetFP();
  if(stackAddrStart == NULL)
  {
    fprintf(stderr, "Garbage Collector Error: Cannot get the stack memory address\n\n");
  }
  else
  {
    stackAddrStart = (int*)*stackAddrStart;
    stackAddrStart = (int*)*stackAddrStart;
    //look for the stack bottom
    stackAddrEnd = stackAddrStart;
    while( *stackAddrEnd != 0)
    {
      stackAddrEnd = (int*)*stackAddrEnd;
    }
    scanMemRangeForHeapPointer(stackAddrStart, stackAddrEnd);
  }

    
  //scan Registers
  // 1) ebx
  regValue = GC_GetEBX();
  scanMemRangeForHeapPointer(&regValue, &regValue);
  // 2) esi
  regValue = GC_GetESI();
  scanMemRangeForHeapPointer(&regValue, &regValue);
  // 3) edi
  regValue = GC_GetEDI();
  scanMemRangeForHeapPointer(&regValue, &regValue);
}

/************************************************************************
 *                         freeUnmarkedBlocks
 * free the blocks that is not marked during garbage collecting
 *************************************************************************/
static void freeUnmarkedBlocks()
{
  int* prevBlockHeaderAddr = NULL;
  int* curBlockHeaderAddr = NULL;
  void (*finalizer)(void*) = NULL;
  
  //sweep
  curBlockHeaderAddr = gHeapStartAddr;
  while(curBlockHeaderAddr < gHeapStartAddr + gHeapSize)
  {
    //free the block that is allocated and unmarked
    if( getAllocBit(curBlockHeaderAddr) == 1 && getMarkBit(curBlockHeaderAddr) == 0 )
    {
      //clear the allocate bit to 0
      clearAllocBit(curBlockHeaderAddr);
      //call the finilize function if the block has one
      if((finalizer = getFinalizer(curBlockHeaderAddr)) != NULL)
      {
        (*finalizer)((void*)(curBlockHeaderAddr+2));
      }
    }
    
    //merge the blocks if both the privous and current blocks are free
    if(prevBlockHeaderAddr != NULL &&
       getAllocBit(prevBlockHeaderAddr) == 0 &&
       getAllocBit(curBlockHeaderAddr) == 0)
    {
      createBlockHeader(prevBlockHeaderAddr,
                        getBlockLen(prevBlockHeaderAddr)+getBlockLen(curBlockHeaderAddr),
                        0);
    }
    else
    {
      prevBlockHeaderAddr = curBlockHeaderAddr;
    }

    //move to the next block
    curBlockHeaderAddr += getBlockLen(curBlockHeaderAddr);
  }
}

/************************************************************************
 *                         clearAllMarkBits
 * clear mark bits of all blocks after garbage collecting
 *************************************************************************/
static void clearAllMarkBits()
{
  int* blockHeaderAddr = NULL;
  
  //sweep and clear the mark bits;
  blockHeaderAddr = gHeapStartAddr;
  while(blockHeaderAddr < gHeapStartAddr + gHeapSize)
  {
    clearMarkBit(blockHeaderAddr);
    blockHeaderAddr += getBlockLen(blockHeaderAddr);
  }
}
/************************************************************************
 *                         collectGarbage
 * identify allocated blocks that are no longer in use and free them
 *************************************************************************/
static void collectGarbage()
{
  markBlocks();
  freeUnmarkedBlocks();
  clearAllMarkBits();
  
}
