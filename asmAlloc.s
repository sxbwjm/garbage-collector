.text
.align 4
.globl GC_GetFP
.globl GC_GetEBX
.globl GC_GetESI
.globl GC_GetEDI

GC_GetFP:
  pushl %ebp
  movl %ebp, %eax
  movl %esp, %ebp
  popl %ebp
  ret

GC_GetEBX:
  movl %ebx, %eax
  ret

GC_GetESI:
  movl %esi, %eax
  ret

GC_GetEDI:
  movl %edi, %eax
  ret

